//
//  GeradorSenha.swift
//  SuperSenha
//
//  Created by Paulo Gutemberg on 28/06/19.
//  Copyright © 2019 Paulo Gutemberg. All rights reserved.
//

import Foundation

class GeradorSenha {
	var numeroDeCaracteres: Int
	var numeroDeSenhas: Int
	var usarLetrasMinusculas: Bool
	var usarNumeros: Bool
	var usarLetrasMaiusculas: Bool
	var usarCaracteresEspeciais: Bool
	
	
	var senhas : [String] = []
	
	private let letras = "abcdefghijklmnopkrstuvwxyz"
	private let especialCaracteres = "!@#$%ˆ&*(){}/?<>[]`˜.,-_:;"
	private let numeros = "0123456789"
	
	init(numeroDeCaracteres: Int, numeroDeSenhas: Int, usarLetrasMinusculas: Bool, usarNumeros: Bool, usarLetrasMaiusculas: Bool, usarCaracteresEspeciais: Bool) {
		
		var numChar = min( numeroDeCaracteres, 16)
		numChar = max(numChar, 1)
		
		self.numeroDeCaracteres = numChar
		self.numeroDeSenhas = numeroDeSenhas
		self.usarLetrasMinusculas = usarLetrasMinusculas
		self.usarNumeros = usarNumeros
		self.usarLetrasMaiusculas = usarLetrasMaiusculas
		self.usarCaracteresEspeciais = usarCaracteresEspeciais
		
	}
	
	func gerador(totalSenhas: Int) -> [String]{
		senhas.removeAll()
		
		var universoDeCaracteresUtilizaveis: String = ""
		
		if usarLetrasMinusculas {
			universoDeCaracteresUtilizaveis = universoDeCaracteresUtilizaveis + letras
		}
		
		if usarNumeros {
			universoDeCaracteresUtilizaveis = universoDeCaracteresUtilizaveis + numeros
		}
		
		if usarCaracteresEspeciais {
			universoDeCaracteresUtilizaveis = universoDeCaracteresUtilizaveis + especialCaracteres
		}
		
		if usarLetrasMaiusculas {
			universoDeCaracteresUtilizaveis = universoDeCaracteresUtilizaveis + letras.uppercased()
		}
		
		let arrayUniverso = Array(universoDeCaracteresUtilizaveis)
		while senhas.count < totalSenhas {
			var senha: String = ""
			for _ in 1...numeroDeCaracteres{
				let indice = Int(arc4random_uniform(UInt32(arrayUniverso.count)))
				senha += String(arrayUniverso[indice])
			}
			senhas.append(senha)
		}
		
		
		return senhas
	}
	
}
