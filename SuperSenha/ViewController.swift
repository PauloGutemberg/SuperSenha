//
//  ViewController.swift
//  SuperSenha
//
//  Created by Paulo Gutemberg on 27/06/19.
//  Copyright © 2019 Paulo Gutemberg. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	@IBOutlet weak var txtTotalSenhas: UITextField!
	@IBOutlet weak var txtTotalCaracteres: UITextField!
	
	
	@IBOutlet weak var swLetraMinuscula: UISwitch!
	@IBOutlet weak var swUsarNumeros: UISwitch!
	
	@IBOutlet weak var swLetrasMaiusculas: UISwitch!
	@IBOutlet weak var swCaracteresEspeciais: UISwitch!
	
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		let senhaViewController = segue.destination as! SenhaViewController
		
		if let numeroDeSenhas = Int(self.txtTotalSenhas.text!) {
			senhaViewController.numeroDeSenhas = numeroDeSenhas
		}
		
		if let numeroDeCaracteres = Int(self.txtTotalCaracteres.text!){
			senhaViewController.numeroDeCaracteres = numeroDeCaracteres
		}
		
		senhaViewController.usarLetrasMinusculas = self.swLetraMinuscula.isOn
		senhaViewController.usarNumeros = self.swUsarNumeros.isOn
		senhaViewController.usarLetrasMaiusculas = self.swLetrasMaiusculas.isOn
		senhaViewController.usarCaracteresEspeciais = self.swCaracteresEspeciais.isOn
		
		//Teclado desaparece, perde o foco
		view.endEditing(true)
	}

}

