//
//  SenhaViewController.swift
//  SuperSenha
//
//  Created by Paulo Gutemberg on 28/06/19.
//  Copyright © 2019 Paulo Gutemberg. All rights reserved.
//

import UIKit

class SenhaViewController: UIViewController {

	@IBOutlet weak var tvSenha: UITextView!
	
	var numeroDeCaracteres: Int = 10
	var numeroDeSenhas: Int = 1
	var usarLetrasMinusculas: Bool!
	var usarNumeros: Bool!
	var usarLetrasMaiusculas: Bool!
	var usarCaracteresEspeciais: Bool!
	
	var geradorDeSenha: GeradorSenha!
	
	override func viewDidLoad() {
        super.viewDidLoad()

		self.title = "Total de senhas: \(self.numeroDeSenhas)"
		geradorDeSenha = GeradorSenha.init(numeroDeCaracteres: self.numeroDeCaracteres, numeroDeSenhas: self.numeroDeSenhas, usarLetrasMinusculas: self.usarLetrasMinusculas, usarNumeros: self.usarNumeros, usarLetrasMaiusculas: self.usarLetrasMaiusculas, usarCaracteresEspeciais: self.usarCaracteresEspeciais)
		
		self.gerarSenhas()
    }
	
	func gerarSenhas(){
		self.tvSenha.scrollRangeToVisible(NSRange(location: 0, length: 0))
		self.tvSenha.text = ""
		
		let senhas = geradorDeSenha.gerador(totalSenhas: self.numeroDeSenhas)
		for senha in senhas {
			self.tvSenha.text.append(senha + "\n\n")
		}
	}
	
	@IBAction func gerarNovamente(_ sender: Any) {
		self.gerarSenhas()
	}
	
}
